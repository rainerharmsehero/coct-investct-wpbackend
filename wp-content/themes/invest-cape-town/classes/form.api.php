<?php

    class form_api(){

        require_once 'classes/google-api-php-client/vendor/autoload.php';

        public function __construct(){
            // include your composer dependencies

            $this->client = new Google_Client();
            $client->setApplicationName("Client_Library_Examples");
            $client->setDeveloperKey("YOUR_APP_KEY");

            $service = new Google_Service_Books($client);
            $optParams = array('filter' => 'free-ebooks');
            $results = $service->volumes->listVolumes('Henry David Thoreau', $optParams);

            foreach ($results as $item) {
            echo $item['volumeInfo']['title'], "<br /> \n";
            }
        }
    
    }