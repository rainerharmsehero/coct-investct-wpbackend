jQuery(function(){

    enable_media_uploader();
    remove_bg_url();
    
});

function remove_bg_url(){
    jQuery("#opportunities_icon_remove_button").on("click", function(e){
        var answer = confirm("Are you sure you want to remove the icon image?")
        if(answer == true){
             jQuery('#opportunities_icon_url_hidden').val("");
             jQuery('#opportunities_icon_img').attr("src", "");
             jQuery('#opportunities_icon_remove_button').hide();
             jQuery('#opportunities_icon_upload_button').show();
        }else{
            console.log("NOOO please DONT delete");
        }
    });
}

function enable_media_uploader(){

    var mediaUploader;

    jQuery("#opportunities_icon_upload_button").on("click", function(e){
        e.preventDefault();
        if(mediaUploader){
            mediaUploader.open();
            return;
        }

        mediaUploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose an icon image',
            button: {
                text: 'Select image',
            },
            multiple: false
        });

        mediaUploader.on('select', function(){
            attachment = mediaUploader.state().get('selection').first().toJSON();
            console.log(attachment.url);
            jQuery('#opportunities_icon_url_hidden').val(attachment.url);
            jQuery('#opportunities_icon_img').attr("src", attachment.url);
            jQuery('#opportunities_icon_upload_button').hide();
            jQuery('#opportunities_icon_remove_button').show();
        });
            
        mediaUploader.open();
    });


}