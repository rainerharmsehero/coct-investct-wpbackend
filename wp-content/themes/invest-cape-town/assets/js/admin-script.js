jQuery(function(){

    add_general_link_page_options_row();
    remove_general_link_page_options_div()
});

var ict_link_option_rows_hidden = jQuery('#ict_link_option_rows_hidden');

function add_general_link_page_options_row(){

    var admin_row_container = jQuery(".general-options-container").html();
    var general_page_options_meta = jQuery('#general-page-link-options-meta > .inside');
    

    //Input field IDs which im going to add number to end
    var row_field_title_content = 'ict_link_title_content';
    var row_field_link_type = 'ict_page_option_link_type';
    var row_field_link_text = 'ict_page_option_link_text';
    var row_field_link = 'ict_page_option_link';

    jQuery("#add-general-options-div").on('click', function(){

        var row_html =  "<div class='general-options-container'>"                     
            row_html +=    "<fieldset>"
            row_html +=         "<select id='ict_page_option_link_type_"+ num_option_rows_state() + "' name='ict_page_option_link_type_" +num_option_rows_state()+"'>"
            row_html +=             "<option value='type' selected='selected'>Type</option>"
            row_html +=             "<option value='email'>Email</option>"
            row_html +=             "<option value'phone'>Phone</option>"
            row_html +=             "<option value='pdf'>PDF</option>"
            row_html +=             "<option value='website'>Website</option>"
            row_html +=         "</select>"
            row_html +=         "<label for='ict_page_option_link_text'>Link Text</label>"
            row_html +=         "<input id='ict_page_option_link_text_"+num_option_rows_state()+"' type='text' name='ict_page_option_link_text_"+num_option_rows_state()+"' value='' class='general-options-input'>"
            row_html +=            "<label for='ict_page_option_link'>Link</label>"
            row_html +=         "<input id='ict_page_option_link_"+num_option_rows_state()+"' type='text' name='ict_page_option_link_"+num_option_rows_state()+"' value='' class='general-options-input'>"
            row_html +=     "</fieldset>"
            row_html +=     "</div>"

        general_page_options_meta.append(row_html);
        console.log("whats happeing");
        jQuery('#ict_link_option_rows_hidden').val(num_option_rows_state() -1);
    });
}

function remove_general_link_page_options_div(){
        jQuery('#remove-general-options-div').on('click', function(){
            jQuery(".general-options-container").last().remove();
        jQuery('#ict_link_option_rows_hidden').val(num_option_rows_state() -1);
    });

}

function num_option_rows_state(){
    var number_of_option_rows = jQuery('.general-options-container').size();
    return number_of_option_rows + 1;
}