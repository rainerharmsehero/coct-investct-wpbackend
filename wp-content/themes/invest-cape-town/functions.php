<?php

/**
 * Invest Cape Town functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Invest Cape Town
 * @since 1.0
 */

// Inlcude Invest Cape Town Admin Settings Page
require_once('inc/function-admin.php');

$textdomain = 'investcapetown';

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function investcapetown_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentyseventeen
	 * If you're building a theme based on Twenty Seventeen, use a find and replace
	 * to change 'twentyseventeen' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( $textdomain );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// ENABLE WIDGETS
	add_theme_support( 'widgets' );


	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'main'   => __( 'Main Menu', 'pearson' ),
		'top'    => __( 'Top Menu', 'pearson' ),
	) );
	

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );

}
add_action( 'after_setup_theme', 'investcapetown_setup' );

/**
 * Enqueue scripts and styles.
 */ 
function investcapetown_scripts(){
    // CSS
	wp_enqueue_style('p_reset', get_template_directory_uri() .'/assets/css/reset.css');
	wp_enqueue_style('p_styles', get_template_directory_uri() .'/assets/css/investcapetown.css', array('p_reset'));

	// JAVASCRIPT
    wp_enqueue_script('p_script', get_template_directory_uri() .'/assets/js/script.js', array('jquery'));
	
	// The wp_localize_script allows us to output the ajax_url path for our script to use.
	wp_localize_script('p_script', 'pearson_ajax_form_obj', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ));
}
add_action( 'wp_enqueue_scripts', 'pearson_scripts' );

function investcapetown_admin_scripts(){

    wp_enqueue_media();
    wp_enqueue_style('p_admin_styles', get_template_directory_uri() .'/assets/css/investcapetown-admin.css');
    wp_enqueue_script('p_admin_script', get_template_directory_uri() .'/assets/js/admin-script.js', array('jquery'));
    wp_enqueue_script('p_media_uploader', get_template_directory_uri() .'/assets/js/media-uploader.js', array('jquery'));

}
add_action( 'admin_enqueue_scripts', 'investcapetown_admin_scripts' );

/*
	==========================================
	 ADMIN PAGE SETTINGS
	=====================x=====================
*/

function investcapetown_add_admin_page(){

	// Generate Invest Cape Town Admin Page
	add_menu_page( 'Invest Cape Town Theme Options', 'Invest Cape Town', 'manage_options',
				   'invest_cape_town', 'investcapetown_theme_create_page', '', 110 );

	// Generate Invest Cape Town Admin Sub Pages
	add_submenu_page( 'invest_cape_town', 'Theme Settings', 'General', 'manage_options', 'invest_cape_town', 'investcapetown_theme_settings_page' );
    //Activate custom settings
    add_action('admin_init', 'investcapetown_theme_settings_page');
}

add_action('admin_menu', 'investcapetown_add_admin_page');


function investcapetown_theme_settings_page(){
    // Generate Invest Cape Town Admin Sub Pages
    register_setting( 'investcapetown-settings-group', 'slider_content' );
    add_settings_section('investcapetown-slider-options', 'Home Page Slider Options', 'investcapetown_slider_options', 'invest_cape_town');
    add_settings_field( 'slider-content', 'Slider Content', 'investcapetown_slider_content', 'invest_cape_town', 'investcapetown-slider-options');

    //Slider Video Links
    register_add_settings_field('investcapetown-settings-group', 'slider_video_link_1',
    'slider-video-link-1','Video Link 1','investcapetown_video_link_1','invest_cape_town', 'investcapetown-slider-options');

    register_add_settings_field('investcapetown-settings-group', 'slider_video_link_2',
    'slider-video-link-2','Video Link 2','investcapetown_video_link_2','invest_cape_town', 'investcapetown-slider-options');

    register_add_settings_field('investcapetown-settings-group', 'slider_video_link_3',
    'slider-video-link-3','Video Link 3','investcapetown_video_link_3','invest_cape_town', 'investcapetown-slider-options');

    register_add_settings_field('investcapetown-settings-group', 'slider_video_link_4',
    'slider-video-link-4','Video Link 4','investcapetown_video_link_4','invest_cape_town', 'investcapetown-slider-options');

    register_add_settings_field('investcapetown-settings-group', 'slider_video_link_5',
    'slider-video-link-5','Video Link 5','investcapetown_video_link_5','invest_cape_town', 'investcapetown-slider-options');

    //Why Invest in Cape Town Metaboxes
    add_settings_section('investcapetown-why-invest-options', 'Why Invest in Cape Town Options', 'investcapetown_why_invest_options', 'why_invest_cape_town');

    register_add_settings_field('investcapetown-settings-group', 'why_invest_1', 
    'why-invest-1', 'Reason 1','investcapetown_reason_1', 'why_invest_cape_town','investcapetown-why-invest-options');

    register_add_settings_field('investcapetown-settings-group', 'why_invest_2', 
    'why-invest-2', 'Reason 2','investcapetown_reason_2', 'why_invest_cape_town','investcapetown-why-invest-options');

    register_add_settings_field('investcapetown-settings-group', 'why_invest_3', 
    'why-invest-3', 'Reason 3','investcapetown_reason_3', 'why_invest_cape_town','investcapetown-why-invest-options');

    register_add_settings_field('investcapetown-settings-group', 'why_invest_4', 
    'why-invest-4', 'Reason 4','investcapetown_reason_4', 'why_invest_cape_town','investcapetown-why-invest-options');

    register_add_settings_field('investcapetown-settings-group', 'why_invest_5', 
    'why-invest-5', 'Reason 5','investcapetown_reason_5', 'why_invest_cape_town','investcapetown-why-invest-options');

    register_add_settings_field('investcapetown-settings-group', 'why_invest_6', 
    'why-invest-6', 'Reason 6','investcapetown_reason_6', 'why_invest_cape_town','investcapetown-why-invest-options');

    register_add_settings_field('investcapetown-settings-group', 'why_invest_7', 
    'why-invest-7', 'Reason 7','investcapetown_reason_7', 'why_invest_cape_town','investcapetown-why-invest-options');

    register_add_settings_field('investcapetown-settings-group', 'why_invest_8', 
    'why-invest-8', 'Reason 8','investcapetown_reason_8', 'why_invest_cape_town','investcapetown-why-invest-options');

    register_add_settings_field('investcapetown-settings-group', 'why_invest_9', 
    'why-invest-9', 'Reason 9','investcapetown_reason_9', 'why_invest_cape_town','investcapetown-why-invest-options');

    //Why invest In Cape Town Link Meta
    register_add_settings_field('investcapetown-settings-group', 'why_invest_link_1', 
    'why-invest-link-1', 'Link 1','investcapetown_link_1', 'why_invest_cape_town','investcapetown-why-invest-options');

    register_add_settings_field('investcapetown-settings-group', 'why_invest_link_2', 
    'why-invest-link-2', 'Link 2','investcapetown_link_2', 'why_invest_cape_town','investcapetown-why-invest-options');

    register_add_settings_field('investcapetown-settings-group', 'why_invest_link_3', 
    'why-invest-link-3', 'Link 3','investcapetown_link_3', 'why_invest_cape_town','investcapetown-why-invest-options');

    register_add_settings_field('investcapetown-settings-group', 'why_invest_link_4', 
    'why-invest-link-4', 'Link 4','investcapetown_link_4', 'why_invest_cape_town','investcapetown-why-invest-options');

    register_add_settings_field('investcapetown-settings-group', 'why_invest_link_5', 
    'why-invest-link-5', 'Link 5','investcapetown_link_5', 'why_invest_cape_town','investcapetown-why-invest-options');

    register_add_settings_field('investcapetown-settings-group', 'why_invest_link_6', 
    'why-invest-link-6', 'Link 6','investcapetown_link_6', 'why_invest_cape_town','investcapetown-why-invest-options');

    register_add_settings_field('investcapetown-settings-group', 'why_invest_link_7', 
    'why-invest-link-7', 'Link 7','investcapetown_link_7', 'why_invest_cape_town','investcapetown-why-invest-options');

    register_add_settings_field('investcapetown-settings-group', 'why_invest_link_8', 
    'why-invest-link-8', 'Link 8','investcapetown_link_8', 'why_invest_cape_town','investcapetown-why-invest-options');

    register_add_settings_field('investcapetown-settings-group', 'why_invest_link_9', 
    'why-invest-link-9', 'Link 9','investcapetown_link_9', 'why_invest_cape_town','investcapetown-why-invest-options');

    
}

function register_add_settings_field($option_group, $option_name, $id, $title, $callback, $page, $section){
    register_setting($option_group, $option_name);
    add_settings_field($id, $title, $callback, $page, $section);
}

function investcapetown_slider_options(){
    echo 'Customize your Slider';
}

function investcapetown_theme_create_page(){
	// Generate Invest Cape Town Admin Page
	require_once('inc/templates/investcapetown-admin.php');

}

/*
	==========================================
	 Investcapetown Settings Options Pages
	=====================x=====================
*/

function investcapetown_slider_content(){
    $slider_content = get_option('slider_content');
    echo '<input type="text" name="slider_content" value="'. $slider_content .'" />';
}

function investcapetown_video_link_1(){
    $slider_video_link_1 = get_option('slider_video_link_1');
    echo '<input type="text" name="slider_video_link_1" value="'. $slider_video_link_1 .'" />';
}

function investcapetown_video_link_2(){
    $slider_video_link_2 = get_option('slider_video_link_2');
    echo '<input type="text" name="slider_video_link_2" value="'. $slider_video_link_2 .'" />';
}

function investcapetown_video_link_3(){
    $slider_video_link_3 = get_option('slider_video_link_3');
    echo '<input type="text" name="slider_video_link_3" value="'. $slider_video_link_3 .'" />';
}

function investcapetown_video_link_4(){
    $slider_video_link_4 = get_option('slider_video_link_4');
    echo '<input type="text" name="slider_video_link_4" value="'. $slider_video_link_4 .'" />';
}

function investcapetown_video_link_5(){
    $slider_video_link_5 = get_option('slider_video_link_5');
    echo '<input type="text" name="slider_video_link_5" value="'. $slider_video_link_5 .'" />';
}


/*
	==========================================
	 WHY Invest reasons Settings Options Pages
	=====================x=====================
*/

function investcapetown_reason_1(){
    $why_invest_1 = get_option('why_invest_1');
    echo '<input type="text" class="" name="why_invest_1" value="'. $why_invest_1 .'" />';
}

function investcapetown_reason_2(){
    $why_invest_2 = get_option('why_invest_2');
    echo '<input type="text" class="" name="why_invest_2" value="'. $why_invest_2 .'" />';
}

function investcapetown_reason_3(){
    $why_invest_3 = get_option('why_invest_3');
    echo '<input type="text" class="" name="why_invest_3" value="'. $why_invest_3 .'" />';
}

function investcapetown_reason_4(){
    $why_invest_4 = get_option('why_invest_4');
    echo '<input type="text" class="" name="why_invest_4" value="'. $why_invest_4 .'" />';
}

function investcapetown_reason_5(){
    $why_invest_5 = get_option('why_invest_5');
    echo '<input type="text" class="" name="why_invest_5" value="'. $why_invest_5 .'" />';
}

function investcapetown_reason_6(){
    $why_invest_6 = get_option('why_invest_6');
    echo '<input type="text" class="" name="why_invest_6" value="'. $why_invest_6 .'" />';
}

function investcapetown_reason_7(){
    $why_invest_7 = get_option('why_invest_7');
    echo '<input type="text" class="" name="why_invest_7" value="'. $why_invest_7 .'" />';
}

function investcapetown_reason_8(){
    $why_invest_8 = get_option('why_invest_8');
    echo '<input type="text" class="" name="why_invest_8" value="'. $why_invest_8 .'" />';
}

function investcapetown_reason_9(){
    $why_invest_9 = get_option('why_invest_9');
    echo '<input type="text" class="" name="why_invest_9" value="'. $why_invest_9 .'" />';
}

//Wht invest Link options
function investcapetown_link_1(){
    $why_invest_link_1 = get_option('why_invest_link_1');
    echo '<input type="text" class="" name="why_invest_link_1" value="'. $why_invest_link_1 .'" />';
}

function investcapetown_link_2(){
    $why_invest_link_2 = get_option('why_invest_link_2');
    echo '<input type="text" class="" name="why_invest_link_2" value="'. $why_invest_link_2 .'" />';
}

function investcapetown_link_3(){
    $why_invest_link_3 = get_option('why_invest_link_3');
    echo '<input type="text" class="" name="why_invest_link_3" value="'. $why_invest_link_3 .'" />';
}

function investcapetown_link_4(){
    $why_invest_link_4 = get_option('why_invest_link_4');
    echo '<input type="text" class="" name="why_invest_link_4" value="'. $why_invest_link_4 .'" />';
}

function investcapetown_link_5(){
    $why_invest_link_5 = get_option('why_invest_link_5');
    echo '<input type="text" class="" name="why_invest_link_5" value="'. $why_invest_link_5 .'" />';
}

function investcapetown_link_6(){
    $why_invest_link_6 = get_option('why_invest_link_6');
    echo '<input type="text" class="" name="why_invest_link_6" value="'. $why_invest_link_6 .'" />';
}

function investcapetown_link_7(){
    $why_invest_link_7 = get_option('why_invest_link_7');
    echo '<input type="text" class="" name="why_invest_link_7" value="'. $why_invest_link_7 .'" />';
}

function investcapetown_link_8(){
    $why_invest_link_8 = get_option('why_invest_link_8');
    echo '<input type="text" class="" name="why_invest_link_8" value="'. $why_invest_link_8 .'" />';
}

function investcapetown_link_9(){
    $why_invest_link_9 = get_option('why_invest_link_9');
    echo '<input type="text" class="" name="why_invest_link_9" value="'. $why_invest_link_9 .'" />';
}



/*
	==========================================
	 REGSITER CUSTOM POST TYPES
	=====================x=====================
*/

// HOME SLIDER CUSTOM POST TYPE
require_once('classes/posttype.php');

$home_banner_slider = new posttype($textdomain, array(
    'post_type' => 'home_banner_slider',
    'singular_name'=> 'Home Banner Slider',
    'plural_name'=> 'Home Banner Sliders',
	'menu_name'=> 'Home Banner Slider',
	'supports'=> array('title', 'editor', 'thumbnail', 'excerpt','page-attributes')
));

$home_slider = new posttype($textdomain, array(
    'post_type' => 'home_slider',
    'singular_name'=> 'Home Slider',
    'plural_name'=> 'Home Sliders',
	'menu_name'=> 'Home Slider',
	'supports'=> array('title', 'editor', 'thumbnail', 'excerpt','page-attributes')
));

// TESTIMONIAL CUSTOM POST TYPE
$testimonial = new posttype($textdomain, array(
    'post_type' => 'testimonial',
    'singular_name'=> 'Testimonial',
    'plural_name'=> 'Testimonials',
	'menu_name'=> 'Testimonials',
	'supports'=> array('title', 'editor', 'thumbnail', 'excerpt','page-attributes')
));

// SUCCESS STORIES CUSTOM POST TYPE
$success_stories = new posttype($textdomain, array(
    'post_type' => 'success_stories',
    'singular_name'=> 'Success Story',
    'plural_name'=> 'Success Stories',
	'menu_name'=> 'Success Stories',
	'supports'=> array('title', 'editor', 'thumbnail', 'excerpt','page-attributes')
));

// INVESTED COMPANIES CUSTOM POST TYPE
$invested_companies = new posttype($textdomain, array(
    'post_type' => 'invested_companies',
    'singular_name'=> 'Invetsed Company',
    'plural_name'=> 'Invested Companies',
	'menu_name'=> 'Invested Companies',
	'supports'=> array('title', 'editor', 'thumbnail', 'excerpt','page-attributes')
));

// AWARDS AND ACCOLADES CUSTOM POST TYPE
$awards_accolades = new posttype($textdomain, array(
    'post_type' => 'awards_accolades',
    'singular_name'=> 'Award and Accolade',
    'plural_name'=> 'Awards and Accolades',
	'menu_name'=> 'Awards and Accolades',
	'supports'=> array('title', 'editor', 'thumbnail', 'excerpt','page-attributes')
));

// INVESTED COMPANIES CUSTOM POST TYPE
$museums = new posttype($textdomain, array(
    'post_type' => 'museums',
    'singular_name'=> 'Museum',
    'plural_name'=> 'Museums',
	'menu_name'=> 'Museums',
	'supports'=> array('title', 'editor', 'thumbnail', 'excerpt','page-attributes')
));

// EVENTS POSTTYPE CUSTOM POST TYPE
$events_posttype = new posttype($textdomain, array(
    'post_type' => 'events_posttype',
    'singular_name'=> 'Event',
    'plural_name'=> 'Events',
	'menu_name'=> 'Events',
	'supports'=> array('title', 'editor', 'thumbnail', 'excerpt','page-attributes')
));

// TEAM MEMBERS CUSTOM POST TYPE
$team_members = new posttype($textdomain, array(
    'post_type' => 'team_members',
    'singular_name'=> 'Team Member',
    'plural_name'=> 'Team Members',
	'menu_name'=> 'Our Team',
	'supports'=> array('title', 'editor', 'thumbnail', 'excerpt','page-attributes')
));

// ONE STOP SHOP CUSTOM POST TYPE
$one_stop_shop = new posttype($textdomain, array(
    'post_type' => 'one_stop_shop',
    'singular_name'=> 'Partner',
    'plural_name'=> 'Partners',
	'menu_name'=> 'One Stop Shop',
	'supports'=> array('title', 'editor', 'thumbnail', 'excerpt','page-attributes')
));

// KEY PARTNERS CUSTOM POST TYPE
$key_partners = new posttype($textdomain, array(
    'post_type' => 'key_partners',
    'singular_name'=> 'Key Partner',
    'plural_name'=> 'Key Partners',
	'menu_name'=> 'Key Partners',
	'supports'=> array('title', 'editor', 'thumbnail', 'excerpt','page-attributes')
));

// KEY PARTNERS CUSTOM POST TYPE
$investment_partners = new posttype($textdomain, array(
    'post_type' => 'investment_partners',
    'singular_name'=> 'Investment Partner',
    'plural_name'=> 'Investment Partners',
	'menu_name'=> 'Investment Partners',
	'supports'=> array('title', 'editor', 'thumbnail', 'excerpt','page-attributes')
));

// VIDEOS & IMAGES CUSTOM POST TYPE
$videos_images = new posttype($textdomain, array(
    'post_type' => 'videos_images',
    'singular_name'=> 'Video or Image',
    'plural_name'=> 'Videos & Images',
	'menu_name'=> 'Videos & Images',
	'supports'=> array('title', 'editor', 'thumbnail', 'excerpt','page-attributes')
));

// FAQs CUSTOM POST TYPE
$faqs = new posttype($textdomain, array(
    'post_type' => 'faqs',
    'singular_name'=> 'FAQ',
    'plural_name'=> 'FAQs',
	'menu_name'=> 'FAQs',
	'supports'=> array('title', 'editor', 'thumbnail', 'excerpt','page-attributes')
));

// PRESENTSTION CUSTOM POST TYPE
$presentation = new posttype($textdomain, array(
    'post_type' => 'presentation',
    'singular_name'=> 'Presentation',
    'plural_name'=> 'Presentations',
	'menu_name'=> 'Presentations',
	'supports'=> array('title', 'editor', 'thumbnail', 'excerpt','page-attributes')
));

// PRESS RELEASES CUSTOM POST TYPE
$press_releases = new posttype($textdomain, array(
    'post_type' => 'press_release',
    'singular_name'=> 'Press Release',
    'plural_name'=> 'Press Releases',
	'menu_name'=> 'Press Releases',
	'supports'=> array('title', 'editor', 'thumbnail', 'excerpt','page-attributes')
));

// BUSINESS EVENTS CUSTOM POST TYPE
$business_events = new posttype($textdomain, array(
    'post_type' => 'business_event',
    'singular_name'=> 'Business Event',
    'plural_name'=> 'Business Events',
	'menu_name'=> 'Business Events',
	'supports'=> array('title', 'editor', 'thumbnail', 'excerpt','page-attributes')
));

// NEWSLETTERS CUSTOM POST TYPE
$newsletters = new posttype($textdomain, array(
    'post_type' => 'newsletter',
    'singular_name'=> 'Newletter',
    'plural_name'=> 'Newletters',
	'menu_name'=> 'Newletters',
	'supports'=> array('title', 'editor', 'thumbnail', 'excerpt','page-attributes')
));

/*
	==========================================
	 CUSTOM POST TYPES TAXONOMIES
	==========================================
*/

// EVENT CATEGORIES TAXONOMY
function events_posttype_taxonomy(){

    $labels = array(
        'name' => 'Events',
        'singular_name' => 'Event',
        'search_items' => 'Search Event',
        'parent_item'  => 'Parent Event',
        'parent_item_colon' => 'Parent Event',
        'edit_item' => 'Edit Event',
        'update_item' => 'Update Event',
        'add_new_item' => 'Add New Event',
        'new_item_name' => 'New Event Name',
        'menu_name'     => 'Event Category',
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'show_ui'   => true,
        'show_admin_column'=> true,
        'query_var' => true,
        'rewrite' => array('slug' => 'events'),
    );


    register_taxonomy('events', 'events_posttype', $args);

}
add_action('init', 'events_posttype_taxonomy');

// ONE STOP SHOP PARTNERS TAXONOMY
function one_stop_shop_taxonomy(){

    $labels = array(
        'name' => 'Level',
        'singular_name' => 'Level',
        'search_items' => 'Search Levels',
        'parent_item'  => 'Parent Level',
        'parent_item_colon' => 'Parent Level',
        'edit_item' => 'Edit Level',
        'update_item' => 'Update Level',
        'add_new_item' => 'Add New Level',
        'new_item_name' => 'New Level Name',
        'menu_name'     => 'Partner Levels',
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'show_ui'   => true,
        'show_admin_column'=> true,
        'query_var' => true,
        'rewrite' => array('slug' => 'one_stop_shop_partners'),
    );


    register_taxonomy('level', 'one_stop_shop', $args);

}
add_action('init', 'one_stop_shop_taxonomy');

// KEY PARTNERS CATEGORIES TAXONOMY
function presentation_sector_taxonomy(){

    $labels = array(
        'name' => 'Sectors',
        'singular_name' => 'Sector',
        'search_items' => 'Search Sector',
        'parent_item'  => 'Parent Sector',
        'parent_item_colon' => 'Parent Sector',
        'edit_item' => 'Edit Sector',
        'update_item' => 'Update Sector',
        'add_new_item' => 'Add New Sector',
        'new_item_name' => 'New Sector Name',
        'menu_name'     => 'Presentation Sectors',
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'show_ui'   => true,
        'show_admin_column'=> true,
        'query_var' => true,
        'rewrite' => array('slug' => 'presentation_sectors'),
    );


    register_taxonomy('sectors', 'presentation', $args);

}
add_action('init', 'presentation_sector_taxonomy');


/*
	==========================================
	 Adding Custom Meta-boxes
	==========================================
*/

//This function initializes the meta box.
function more_info_home_slider(){
    add_meta_box (
        'home-slider-more-info-meta',
        'More Info Link',
		'display_more_info_home_slider',
		'home_slider',
		'normal'
    );
}

function display_more_info_home_slider($post){
	wp_nonce_field( 'slider_more_info_action', 'slider_more_info_nonce');
	
	$slider_more_info_link = get_post_meta($post->ID,'slider_more_info_link', true );

	?>
		<input id="slider_more_info_link" class="widefat" type='text' name='slider_more_info_link'  value='<?php echo $slider_more_info_link; ?>' ><br>
	
	<?php
}

function save_home_more_info_meta($post_id){
    // var_dump($_POST);
    // wp_die("dead");
	// Check if nonces are set.
    if( ! investcapetown__verify_nonce($_POST['slider_more_info_nonce'], 'slider_more_info_action') ){
        return;
    }
    // Check if user has permissions to save data.
    if ( ! current_user_can( 'edit_post', $post_id ) ) {
        return;
    }
    // Check if not an autosave.
    if ( wp_is_post_autosave( $post_id ) ) {
        return;
    }
    // Check if not a revision.
    if ( wp_is_post_revision( $post_id ) ) {
        return;
	}
	
	// If form fields exisit then add to DB
    if(isset($_POST['slider_more_info_link'])){
        update_post_meta($post_id, 'slider_more_info_link', esc_attr($_POST['slider_more_info_link']));
	}
}
add_action('save_post', 'save_home_more_info_meta');

add_action( 'add_meta_boxes', 'more_info_home_slider' );

//This function initializes the meta box.
function add_opportunities_link_icon(){
    add_meta_box (
        'opportunities-link-icon',
        'Add Opportunities Link and Icon',
		'display_opportunities_link_icon',
		'page',
		'normal'
    );
}

function display_opportunities_link_icon($post){
	wp_nonce_field( 'opportunities_link_action', 'opportunities_link_nonce');
	
	$opportunities_link = get_post_meta($post->ID,'opportunities_link', true );
    $opportunities_icon_url = get_post_meta($post->ID,'opportunities_icon_url_hidden', true );

    ?>
    <input id="opportunities_link" class="" type='text' name='opportunities_link'  value='<?php echo $opportunities_link; ?>' ><br>
    <div class="opportunites-icon-button-container">
    <?php
    if(!empty($opportunities_icon_url)){
        //handle success contains url
        ?> 
            <input id="opportunities_icon_upload_button" class="opportunity-admin-button hide-me" type='button' name='opportunities_icon_url'  value='Select Icon' ><br>
            <input id="opportunities_icon_remove_button" class="opportunity-admin-button" type='button' name='opportunities_icon_url'  value='Remove Icon' ><br>

        <?php
    }else{
        //handle empty
        ?>
            <input id="opportunities_icon_upload_button" class="opportunity-admin-button" type='button' name='opportunities_icon_url'  value='Select Icon' ><br>
            <input id="opportunities_icon_remove_button" class="opportunity-admin-button hide-me" type='button' name='opportunities_icon_url'  value='Remove Icon' ><br>
        <?php
    }
    ?>
    </div>
    <?php

	?>
        <input id="opportunities_icon_url_hidden" class="" type='hidden' name='opportunities_icon_url_hidden'  value='' ><br>
        <img id="opportunities_icon_img" src="<?php echo $opportunities_icon_url; ?>"/>
	<?php
    
}

function save_opportunities_link_icon($post_id){
	// Check if nonces are set.
    if( ! investcapetown__verify_nonce($_POST['opportunities_link_nonce'], 'opportunities_link_action') ){
        return;
    }
    // Check if user has permissions to save data.
    if ( ! current_user_can( 'edit_post', $post_id ) ) {
        return;
    }
    // Check if not an autosave.
    if ( wp_is_post_autosave( $post_id ) ) {
        return;
    }
    // Check if not a revision.
    if ( wp_is_post_revision( $post_id ) ) {
        return;
    }
    // If form fields exisit then add to DB
    if(isset($_POST['opportunities_icon_url_hidden'])){
        update_post_meta($post_id, 'opportunities_icon_url_hidden', esc_attr($_POST['opportunities_icon_url_hidden']));
	}
	
	// If form fields exisit then add to DB
    if(isset($_POST['opportunities_link'])){
        update_post_meta($post_id, 'opportunities_link', esc_attr($_POST['opportunities_link']));
	}
}
add_action('save_post', 'save_opportunities_link_icon');

add_action( 'add_meta_boxes', 'add_opportunities_link_icon' );


//General Page Post Type OPtions Meta Box
//This function initializes the meta box.
function general_page_posttype_options(){
    add_meta_box (
        'general-page-link-options-meta',
        'General Page Link Options',
		'display_general_link_page_options',
		'page',
		'normal'
    );
}

function display_general_link_page_options($post){
	wp_nonce_field( 'general_page_link_options_action', 'general_page_link_options_nonce');
	
    $ict_link_title_content = get_post_meta($post->ID,'ict_link_title_content', true );
    $ict_page_option_link_text = get_post_meta($post->ID,'ict_page_option_link_text', true );
    $ict_page_option_link = get_post_meta($post->ID,'ict_page_option_link', true );
    $ict_page_option_link_type = get_post_meta($post->ID,'ict_page_option_link_type', true );

    $ict_link_options_rows = get_post_meta($post->ID,'ict_link_options_rows', true );
    $counter = 1;
    var_dump($ict_link_options_rows);


	?>
        <label for="">Title</label>
		<input id="ict_link_title_content" class="widefat" type='text' name='ict_link_title_content'  value='<?php echo $ict_link_title_content; ?>' ><br>

        <input id="remove-general-options-div" type="button" name="" value="Remove row -" class="button general-page-link-options-button"/>
        <input id="add-general-options-div" type="button" name="" value="Add row +" class="button general-page-link-options-button"/>

        <input id="ict_link_option_rows_hidden" class="" type='hidden' name='ict_link_option_rows_hidden'  value='' ><br>

        <?php if(!empty($ict_link_options_rows)): ?>
            <?php foreach ($ict_link_options_rows as $row): ?>
                <div class="general-options-container">
                    <fieldset>
                        <select id="ict_page_option_link_type_<?php echo $counter; ?>" name="ict_page_option_link_type_<?php echo $counter; ?>">
                            <option value="type" <?php selected( $row['type'], 'type' ); ?>>Type</option>
                            <option value="email" <?php selected( $row['type'], 'email' ); ?>>Email</option>
                            <option value="phone" <?php selected( $row['type'], 'phone' ); ?>>Phone</option>
                            <option value="pdf" <?php selected( $row['type'], 'pdf' ); ?>>PDF</option>
                            <option value="website" <?php selected( $row['type'], 'website' ); ?>>Website</option>
                        </select>
                        <label for="">Link Text</label>
                        <input id="ict_page_option_link_text_<?php echo $counter; ?>" type="text" name="ict_page_option_link_text_<?php echo $counter; ?>" value="<?php echo $row['text']; ?>" class="general-options-input"/>
                        <label for="">Link</label>
                        <input id="ict_page_option_link_<?php echo $counter; ?>" type="text" name="ict_page_option_link_<?php echo $counter; ?>" value="<?php echo $row['link']; ?>" class="general-options-input"/>
                    </fieldset>
                </div>
                <?php $counter++; ?>
            <?php endforeach; ?>
        <?php endif; ?>
	<?php
}

function save_general_page_options_meta($post_id){
    // var_dump($_POST);
    // wp_die("dead");
	// Check if nonces are set.
    if( ! investcapetown__verify_nonce($_POST['general_page_link_options_nonce'], 'general_page_link_options_action') ){
        return;
    }
    // Check if user has permissions to save data.
    if ( ! current_user_can( 'edit_post', $post_id ) ) {
        return;
    }
    // Check if not an autosave.
    if ( wp_is_post_autosave( $post_id ) ) {
        return;
    }
    // Check if not a revision.
    if ( wp_is_post_revision( $post_id ) ) {
        return;
    }
    // If form fields exisit then add to DB
    if(isset($_POST['ict_link_title_content'])){
        update_post_meta($post_id, 'ict_link_title_content', esc_attr($_POST['ict_link_title_content']));
    }

    if(isset($_POST['ict_link_option_rows_hidden'])){
        if($_POST['ict_link_option_rows_hidden'] > 0){
            $link_option_rows = array();
            for($i = 1; $i <= $_POST['ict_link_option_rows_hidden']; $i++){
                $row = array(
                    'type' => (isset($_POST['ict_page_option_link_type_'. $i])) ? $_POST['ict_page_option_link_type_'. $i] : '',
                    'text' => (isset($_POST['ict_page_option_link_text_'. $i])) ? $_POST['ict_page_option_link_text_'. $i] : '',
                    'link' => (isset($_POST['ict_page_option_link_'. $i])) ? $_POST['ict_page_option_link_'. $i] : ''
                );
                array_push($link_option_rows, $row);
            }
            update_post_meta($post_id, 'ict_link_options_rows', $link_option_rows);
        } else {
            update_post_meta($post_id, 'ict_link_options_rows', '');
        }
    }




}
add_action('save_post', 'save_general_page_options_meta');

add_action( 'add_meta_boxes', 'general_page_posttype_options' );




// ============ END PARSING PROGRAMMES MODULES CONTENT =======//


// ========== HANDLE AJAX FORM SUMBIT ============ //

// function example_ajax_request() {
 
//     // The $_REQUEST contains all the data sent via ajax
//     if ( isset($_POST) ) {
     
// 		$form_data = $_POST['form_data'];

// 		// Now we'll return it to the javascript function
// 		// Anything outputted will be returned in the response
// 		$response['success'] = true;
// 		$response['payload'] = $form_data;
		
//     }
//     // Always die in functions echoing ajax content
//    wp_die();
// }

// add_action( 'wp_ajax_example_ajax_request', 'example_ajax_request' );
// // If you wanted to also use the function for non-logged in users (in a theme for example)
// add_action( 'wp_ajax_nopriv_example_ajax_request', 'example_ajax_request' );

// ========== HANDLE AJAX FORM SUMBIT END ============ //




// =========   Util functions ========= //

function investcapetown__verify_nonce($nonce_name, $nonce_action){
    // Check if nonce is set.
    if ( ! isset( $nonce_name ) ) {
        return false;
    }
    // Check if nonce is valid.
    if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) ) {
        return false;
    }
    return true;
}




