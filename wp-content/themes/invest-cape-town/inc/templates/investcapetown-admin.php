<h1>Invest Cape Town Theme Options</h1>
<?php settings_errors(); ?>
<form method="post" action="options.php">
    <?php settings_fields('investcapetown-settings-group'); ?>
    <?php do_settings_sections('invest_cape_town'); ?>
    <div class="why-invest-ct why-ct-link">
        <?php do_settings_sections('why_invest_cape_town'); ?>
    </div>
    <?php submit_button(); ?>
</form>

<fieldset>
    <legend>Row 1:</legend>
    <select id='ict_page_option_link_type' name='ict_page_option_link_type';>
        <option selected='selected'>Type</option>
        <option value='email'>Email</option>
        <option value'telephone'>Telephone</option>
        <option value='pdf'>PDF</option>
        <option value='youtube'>YouTube</option>
        <option value='extra'>Extra</option>
    </select>
    <label for='ict_page_option_link_text'>Link Text</label>
    <input id='ict_page_option_link_text' type='text' name='ict_page_option_link_text' value='' class='general-options-input'>
    <label for='ict_page_option_link'>Link</label>
    <input id='ict_page_option_link' type='text' name='ict_page_option_link' value='' class='general-options-input'>
</fieldset>