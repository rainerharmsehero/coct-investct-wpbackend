<?php

class ppf_map_meta {
    private $screen;

    public function __construct(){
        $this->screen = array('page');

        add_action('add_meta_boxes', array($this, 'register_map_meta'));
        add_action('save_post', array($this, 'save_map_meta'), 10, 2);
    }

    /**
     * register_slider_meta
     */
    public function register_map_meta(){
        add_meta_box('map-meta', __('Map', 'peaceparks'), array($this, 'render_map_section'), $this->screen,'normal');
    }

    public function render_map_section($post){
        wp_nonce_field( 'map_section_action', 'map_section_nonce' );

        $map = get_post_meta($post->ID, 'map', true);
        $map_thumbnail = get_post_meta($post->ID, 'map-thumbnail', true);
        $title = get_post_meta($post->ID, 'map-title', true);
        $content = get_post_meta($post->ID, 'map-content', true);
        $legends = get_post_meta($post->ID, 'map-legends', true);
        /*echo '<pre>';
        print_r($legends);
        echo '</pre>';*/
        $counter = 1;

        ?>
        <div class="ppf-conservation-area-meta ppf-enable-image-uploads">
            <p><label for="ppf-map-title"><?php _e('Map Title', 'peaceparks'); ?></label></p>
            <input type="text" name="ppf-map-title" id="ppf-map-title" class="widefat" value="<?php echo esc_attr($title); ?>">
            <div class="ppf-conservation-area-content">
                <p><label for="ppf-map-content"><?php _e('Map Content', 'peaceparks'); ?></label></p>
                <textarea name="ppf-map-content" id="ppf-map-content"><?php echo esc_attr($content); ?></textarea>
                <p><?php _e('Legends', 'peaceparks'); ?></p>
                <div class="ppf-conservation-map-legends">
                    <?php if(!empty($legends)): ?>
                        <?php foreach($legends as $legend): ?>
                            <div class="ppf-conservation-map-legend">
                                <div class="ppf-conservation-map-legend-icon">
                                    <?php if(!empty($legend['image'])): ?>
                                        <div class="pp-image image-attached" style="background-image: url('<?php echo esc_url($legend['thumbnail']); ?>');"></div>
                                    <?php else: ?>
                                        <div class="pp-image"></div>
                                    <?php endif; ?>
                                    <div class="ppf-image-upload-buttons">
                                        <?php if(!empty($legend['image'])): ?>
                                            <a href="" class="button pp-add-image"><?php _e('Change Image', 'peaceparks'); ?></a>
                                            <a href="" class="button pp-remove-image"><?php _e('Remove Image', 'peaceparks'); ?></a>
                                        <?php else: ?>
                                            <a href="" class="button pp-add-image"><?php _e('Add Image', 'peaceparks'); ?></a>
                                            <a href="" class="button pp-remove-image hidden"><?php _e('Remove Image', 'peaceparks'); ?></a>
                                        <?php endif; ?>
                                        <input type="hidden" name="ppf-legend-icon-<?php echo $counter; ?>" id="ppf-legend-icon-<?php echo $counter; ?>" class="ppf-image-url" value="<?php echo esc_url($legend['image']); ?>">
                                        <input type="hidden" name="ppf-legend-thumbnail-<?php echo $counter; ?>" id="ppf-legend-thumbnail-<?php echo $counter; ?>" class="ppf-thumbnail-url" value="<?php echo esc_url($legend['thumbnail']); ?>">
                                    </div>
                                </div>
                                <div class="ppf-conservation-map-legend-title">
                                    <p><label for="ppf-legend-title-<?php echo $counter; ?>" class="ppf-legend-title-label"><?php _e('Legend Name', 'peaceparks'); ?></label></p>
                                    <input type="text" name="ppf-legend-title-<?php echo $counter; ?>" id="ppf-legend-title-<?php echo $counter; ?>" class="widefat ppf-legend-title-input" value="<?php echo esc_attr($legend['name']); ?>">
                                </div>
                                <div class="clear"></div>
                                <a href="" class="button button-small pp-remove-legend"><?php _e('Remove Legend', 'peaceparks'); ?></a>
                            </div>
                            <?php $counter++; ?>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div class="ppf-conservation-map-legend">
                            <div class="ppf-conservation-map-legend-icon">
                                <div class="pp-image"></div>
                                <div class="ppf-image-upload-buttons">
                                    <?php if(!empty($map)): ?>
                                        <a href="" class="button pp-add-image"><?php _e('Change Image', 'peaceparks'); ?></a>
                                        <a href="" class="button pp-remove-image"><?php _e('Remove Image', 'peaceparks'); ?></a>
                                    <?php else: ?>
                                        <a href="" class="button pp-add-image"><?php _e('Add Image', 'peaceparks'); ?></a>
                                        <a href="" class="button pp-remove-image hidden"><?php _e('Remove Image', 'peaceparks'); ?></a>
                                    <?php endif; ?>
                                    <input type="hidden" name="ppf-legend-icon-<?php echo $counter; ?>" id="ppf-legend-icon-<?php echo $counter; ?>" class="ppf-image-url" value="">
                                    <input type="hidden" name="ppf-legend-thumbnail-<?php echo $counter; ?>" id="ppf-legend-thumbnail-<?php echo $counter; ?>" class="ppf-thumbnail-url" value="">
                                </div>
                            </div>
                            <div class="ppf-conservation-map-legend-title">
                                <p><label for="ppf-legend-title-<?php echo $counter; ?>" class="ppf-legend-title-label"><?php _e('Legend Name', 'peaceparks'); ?></label></p>
                                <input type="text" name="ppf-legend-title-<?php echo $counter; ?>" id="ppf-legend-title-<?php echo $counter; ?>" class="widefat ppf-legend-title-input" value="">
                            </div>
                            <div class="clear"></div>
                            <a href="" class="button button-small pp-remove-legend"><?php _e('Remove Legend', 'peaceparks'); ?></a>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="pp-meta-settings last">
                    <a href="" class="button add-legend"><?php _e('Add Legend', 'peaceparks'); ?></a>
                    <input type="hidden" name="amount-of-legends" id="amount-of-legends" value="<?php echo esc_attr($counter - 1); ?>">
                </div>
            </div>
            <div class="ppf-conservation-area-map">
                <p><label for="ppf-map"><?php _e('Map', 'peaceparks'); ?></label></p>
                <?php if(!empty($map)): ?>
                    <div class="pp-image image-attached" style="background-image: url('<?php echo esc_url($map_thumbnail); ?>');"></div>
                <?php else: ?>
                    <div class="pp-image"></div>
                <?php endif; ?>
                <div class="ppf-image-upload-buttons">
                    <?php if(!empty($map)): ?>
                        <a href="" class="button pp-add-image"><?php _e('Change Image', 'peaceparks'); ?></a>
                        <a href="" class="button pp-remove-image"><?php _e('Remove Image', 'peaceparks'); ?></a>
                    <?php else: ?>
                        <a href="" class="button pp-add-image"><?php _e('Add Image', 'peaceparks'); ?></a>
                        <a href="" class="button pp-remove-image hidden"><?php _e('Remove Image', 'peaceparks'); ?></a>
                    <?php endif; ?>
                    <input type="hidden" name="map-image" id="map-image" class="ppf-image-url" value="<?php echo esc_url($map); ?>">
                    <input type="hidden" name="map-thumbnail" id="map-thumbnail" class="ppf-thumbnail-url" value="<?php echo esc_url($map_thumbnail); ?>">
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <?php
    }

    public function save_map_meta($post_id){
        // Check if nonces are set.
        if( !$this->pp_verify_nonce($_POST['map_section_nonce'], 'map_section_action') ){
            return;
        }
        // Check if user has permissions to save data.
        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
        // Check if not an autosave.
        if ( wp_is_post_autosave( $post_id ) ) {
            return;
        }
        // Check if not a revision.
        if ( wp_is_post_revision( $post_id ) ) {
            return;
        }

        if(isset($_POST['ppf-map-title'])){
            update_post_meta($post_id, 'map-title', esc_attr($_POST['ppf-map-title']));
        }
        if(isset($_POST['ppf-map-content'])){
            update_post_meta($post_id, 'map-content', esc_attr($_POST['ppf-map-content']));
        }
        if(isset($_POST['map-image'])){
            update_post_meta($post_id, 'map', esc_url($_POST['map-image']));
        }
        if(isset($_POST['map-thumbnail'])){
            update_post_meta($post_id, 'map-thumbnail', esc_url($_POST['map-thumbnail']));
        }
        if(isset($_POST['amount-of-legends'])){
            if($_POST['amount-of-legends'] > 0){
                $legends = array();
                for($i = 1; $i <= $_POST['amount-of-legends']; $i++){
                    $legend = array(
                        'name' => (isset($_POST['ppf-legend-title-'. $i])) ? $_POST['ppf-legend-title-'. $i] : '',
                        'image' => (isset($_POST['ppf-legend-icon-'. $i])) ? $_POST['ppf-legend-icon-'. $i] : '',
                        'thumbnail' => (isset($_POST['ppf-legend-thumbnail-'. $i])) ? $_POST['ppf-legend-thumbnail-'. $i] : ''
                    );
                    array_push($legends, $legend);
                }
                update_post_meta($post_id, 'map-legends', $legends);
            } else {
                update_post_meta($post_id, 'map-legends', '');
            }
        }
    }

    private function pp_verify_nonce($nonce_name, $nonce_action){
        // Check if nonce is set.
        if ( ! isset( $nonce_name ) ) {
            return false;
        }
        // Check if nonce is valid.
        if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) ) {
            return false;
        }
        return true;
    }
}