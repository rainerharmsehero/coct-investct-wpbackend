<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'invest_cape_town');

/** MySQL database username */
define('DB_USER', 'admin');

/** MySQL database password */
define('DB_PASSWORD', 'password');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'k=@A:K#|2n+C3;wYOjwY?*TMqXU=B-=9x;n3R|Dj{Na<NE:?:Uqz1u?f<=q/8N+S');
define('SECURE_AUTH_KEY',  'FF*-=mCs70K9Ok6@`NDA8qLn{9-qq*K9*)zfN#^hC~d3M6P1ri57WwTLynYSRX*0');
define('LOGGED_IN_KEY',    'XB?#M&H>5bO/n]HQO6MG)@_lk`00xt(}-9wyZ8rBp1SSw+%=#N(,r]wf,bNe|0!?');
define('NONCE_KEY',        'zgdLjqW-f$D-aK*vsT,5Rqe 9-l-tt|=f`ko]k~j/d34UAXR~IWCfqRu&cjf2j/0');
define('AUTH_SALT',        '?fqa.=KbTzkD3BAiQb$72zo;IIQG~d$pe#KP;.bPY.aI.o85]ypWz:4W;ST+A(b^');
define('SECURE_AUTH_SALT', 't/DszhrMLMX:E,%i$Gt#a}@wyn|9Z:14l1pf29*wy|2-Pl-f=9fU27;n^oM6.QMz');
define('LOGGED_IN_SALT',   'bLLa~e(4an!%_TsYK.}|f`zal<>^40Zs%0B},}N5coZvM+2@g{)asM#B!!C2zx%~');
define('NONCE_SALT',       'x{F)6:edk4qZdhBz3!J<5xpyov-}fdol1%n]O-b@kxaaUDwCOfrK2&!VdMDjLf70');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
